import { checkPropTypes } from "prop-types";

export const checkProps = (component, expectedProps) => {
  // eslint-disable-next-line react/forbid-foreign-prop-types
  const propsError = checkPropTypes(component.propTypes, expectedProps, "props", component.name);
  return propsError;
};

export const dataTestAttributeFinder = (wrapper, attribute) => {
  const element = wrapper.find(`[data-test='${attribute}']`);
  return element;
};
