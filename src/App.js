import React from 'react';
import Header from './components/core/Header';
import FormContent from './components/fizzbuzz/FormContent';
import ListItem from './components/fizzbuzz/ListItem';
import { FizzBuzzConstants } from './utilities/fizz-buzz-constants';

import './App.css';

const { headerText } = FizzBuzzConstants;

const App = () => {
  return (
    <div className="container pb-5">
      <Header heading={headerText} />
      <FormContent />
      <ListItem />
    </div>
  );
};

export default App;
