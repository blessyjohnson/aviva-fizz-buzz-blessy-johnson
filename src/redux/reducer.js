import * as actions from './actionTypes';

//InitialState

const initialState = {
  currentPage: 1,
  currentDisplayCount: 0,
};

//Reducer

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.COMPUTE_FIZZ_VALUES:
      return { ...state, computedValues: action.payload };

    case actions.RESET_FIZZ_VALUES:
      return initialState;

    case actions.CURRENT_PAGE_VALUE:
      return { ...state, currentPage: action.payload };

    case actions.CURRENT_DISPLAY_COUNT:
      return { ...state, currentDisplayCount: action.payload };

    default:
      return state;
  }
};
