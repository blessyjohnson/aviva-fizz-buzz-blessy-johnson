import * as actions from './actionTypes';

//Action Creators
export const computeValues = (data) => {
  return {
    type: actions.COMPUTE_FIZZ_VALUES,
    payload: data,
  };
};

export const resetValues = () => {
  return {
    type: actions.RESET_FIZZ_VALUES,
  };
};

export const setCurrentPage = (data) => {
  return {
    type: actions.CURRENT_PAGE_VALUE,
    payload: data,
  };
};

export const setcurrentDisplayCount = (data) => {
  return {
    type: actions.CURRENT_DISPLAY_COUNT,
    payload: data,
  };
};
