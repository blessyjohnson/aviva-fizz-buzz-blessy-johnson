import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import Header from './components/core/Header';
import FormContent from './components/fizzbuzz/FormContent';
import ListItem from './components/fizzbuzz/ListItem';

describe('App Component :', () => {
  const wrapper = shallow(<App />);

  it('Should render Header Component', () => {
    const headerComponent = wrapper.find(Header);
    expect(headerComponent.exists()).toBe(true);
  });

  it('Should render Form Component', () => {
    const formComponent = wrapper.find(FormContent);
    expect(formComponent.exists()).toBe(true);
  });

  it('Should render ListItem Component', () => {
    const listItemComponent = wrapper.find(ListItem);
    expect(listItemComponent.exists()).toBe(true);
  });
});
