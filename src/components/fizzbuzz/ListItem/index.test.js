import React from 'react';
import { shallow, mount } from 'enzyme';
import ListItem, { mapDispatchToProps } from './index';
import { checkProps, dataTestAttributeFinder } from './../../../../Utils';
import { setCurrentPage, setcurrentDisplayCount } from '../../../redux/action';
import Button from './../../core/Button';
import '../../../setupTests';

// Create the mock store
import configureMockStore from 'redux-mock-store';
const mockStore = configureMockStore();

const initialState = {
  fbReducer: {
    currentPage: 7,
    currentDisplayCount: 7,
    displaySequence: [
      { key: 1, value: 1 },
      { key: 2, value: 2 },
    ],
  },
};

const store = mockStore(initialState);

describe('List Item', () => {
  describe('Checking Proptypes', () => {
    it('should not throw warning', () => {
      const expectedProps = {
        displayCount: 1,
        val: [
          {
            key: 1,
            value: 11,
          },
        ],
      };
      const propsError = checkProps(ListItem, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Checking props', () => {
    const props = {
      currentDisplayCount: 0,
      currentPage: 1,
      displayCount: 20,
      displaySequence: [
        { key: 1, value: 1 },
        { key: 2, value: 2 },
        { key: 3, value: 'fizz' },
      ],
      setCurrentPage: jest.fn(),
      setcurrentDisplayCount: jest.fn(),
      val: undefined,
    };

    it('Should render list of values : Have props', () => {
      const renderComponent = () => shallow(<ListItem store={store} {...props} />).dive();

      const dispatchMock = jest.fn();
      const component = renderComponent({ dispatch: dispatchMock });

      component.find(`[data-test='listGroup']`);
      expect(component.length).toEqual(1);
    });

    it('Should not render list of values : Have no props', () => {
      const renderComponent = (props = undefined) =>
        shallow(<ListItem store={store} {...props} />).dive();

      const dispatchMock = jest.fn();
      const component = renderComponent({ dispatch: dispatchMock });

      const listElement = dataTestAttributeFinder(component, 'listGroup');
      expect(listElement.length).toEqual(0);
    });
  });

  describe('Checking Functions of ListItem : Pagination function: Previous page', () => {
    const props = {
      currentDisplayCount: 3,
      currentPage: 1,
      displayCount: 20,
      displaySequence: [
        { key: 1, value: 1 },
        { key: 2, value: 2 },
        { key: 3, value: 'fizz' },
      ],
      val: [
        { key: 1, value: 1 },
        { key: 2, value: 2 },
      ],
      setCurrentPage: jest.fn(),
      setcurrentDisplayCount: jest.fn(),
      val: undefined,
    };

    const initialState = {
      fbReducer: {
        currentPage: 3,
        currentDisplayCount: 40,
        computedValues: [
          { key: 1, value: 1 },
          { key: 2, value: 2 },
        ],
      },
    };

    const store = mockStore(initialState);

    it('should simulate a button click for previous', () => {
      const wrapper = mount(<ListItem store={store} />);
      wrapper.find(Button).first().simulate('click');
      expect(wrapper.find('ListItem').props().store.getActions()[0].payload).toEqual(2);
    });
  });

  describe('Checking Functions of ListItem : Pagination function: Next page', () => {
    const props = {
      currentDisplayCount: 3,
      currentPage: 1,
      displayCount: 20,
      displaySequence: [
        { key: 1, value: 1 },
        { key: 2, value: 2 },
        { key: 3, value: 'fizz' },
      ],
      setCurrentPage: jest.fn(),
      setcurrentDisplayCount: jest.fn(),
      val: undefined,
    };

    const initialState = {
      fbReducer: {
        currentPage: 3,
        currentDisplayCount: 40,
        computedValues: [
          { key: 1, value: 1 },
          { key: 2, value: 2 },
        ],
      },
    };

    const store = mockStore(initialState);

    it('should simulate a button click for next page', () => {
      const wrapper = mount(<ListItem store={store} />);
      wrapper.find(Button).last().simulate('click');
      expect(wrapper.find('ListItem').props().store.getActions()[0].payload).toEqual(4);
    });
  });

  describe('Checking mapDispatchToProps', () => {
    const dispatch = jest.fn();

    const data = 1;

    it('it should call correct dispatches', () => {
      const actualValue = mapDispatchToProps(dispatch);
      expect(Object.keys(actualValue)).toEqual(['setCurrentPage', 'setcurrentDisplayCount']);

      actualValue.setCurrentPage(data);
      expect(dispatch).toBeCalledWith(setCurrentPage(data));

      actualValue.setcurrentDisplayCount(data);
      expect(dispatch).toBeCalledWith(setcurrentDisplayCount(data));
    });
  });
});
