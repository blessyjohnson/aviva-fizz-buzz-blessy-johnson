import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Pagination from '../../core/Pagination';
import { FizzBuzzConstants } from '../../../utilities/fizz-buzz-constants';
import { setCurrentPage, setcurrentDisplayCount } from '../../../redux/action';

const { displayCount, previousText, nextText } = FizzBuzzConstants;

const propTypes = {
  displayCount: PropTypes.number.isRequired,
  currentDisplayCount: PropTypes.number,
  displaySequence: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
  ),
  val: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number,
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    }),
  ),
};

const defaultProps = {
  displayCount: 20,
  currentDisplayCount: 0,
  displaySequence: undefined,
  val: undefined,
};

const ListItem = (props) => {
  const pageCount = () => {
    const pageCount =
      props.displaySequence !== undefined
        ? Math.ceil(props.displaySequence.length / displayCount)
        : '';
    return pageCount;
  };

  const previousPage = (currentPage, currentDisplayCount) => {
    props.setCurrentPage(currentPage - 1);
    props.setcurrentDisplayCount(currentDisplayCount - displayCount);
  };

  const nextPage = (currentPage, currentDisplayCount) => {
    props.setCurrentPage(currentPage + 1);
    props.setcurrentDisplayCount(currentDisplayCount + displayCount);
  };

  const { currentDisplayCount, displaySequence, currentPage } = props;
  let array = [];

  array =
    displaySequence !== undefined
      ? displaySequence.slice(currentDisplayCount, currentDisplayCount + displayCount)
      : null;

  return displaySequence !== undefined ? (
    <div>
      <p>The Fizz Buzz values generated are:</p>
      <ul className="list-group" data-test="listGroup">
        {array.map((values) => (
          <li key={values.key} className={`list-group-item w-25 p-2`}>
            {typeof values.value == 'string' ? (
              values.value.split(' ').map((value, index) => {
                return (
                  <span key={index} className={value}>
                    {value}
                  </span>
                );
              })
            ) : (
              <span>{values.value}</span>
            )}
          </li>
        ))}
      </ul>
      <Pagination
        previousPage={() => previousPage(currentPage, currentDisplayCount)}
        nextPage={() => nextPage(currentPage, currentDisplayCount)}
        pageCount={pageCount()}
        currentPage={currentPage}
        previousText={previousText}
        nextText={nextText}
      />
    </div>
  ) : null;
};

ListItem.propTypes = propTypes;
ListItem.defaultProps = defaultProps;

const mapStateToProps = (state) => ({
  currentDisplayCount: state.fbReducer.currentDisplayCount,
  displaySequence: state.fbReducer.computedValues,
  currentPage: state.fbReducer.currentPage,
});

export const mapDispatchToProps = (dispatch) => ({
  setCurrentPage: (data) => {
    dispatch(setCurrentPage(data));
  },
  setcurrentDisplayCount: (data) => {
    dispatch(setcurrentDisplayCount(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);
