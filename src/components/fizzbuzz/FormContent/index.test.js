import React, { useState as useStateMock } from 'react';
import { shallow, mount } from 'enzyme';
import FormContent, { mapDispatchToProps } from './index';
import { checkProps, dataTestAttributeFinder } from '../../../../Utils';
import IntegerInput from '../../core/IntegerInput';
import Button from './../../core/Button';
import * as actions from '../../../redux/action';
import { resetValues, computeValues } from './../../../redux/action';
import * as types from '../../../redux/actionTypes';
import { FizzBuzzValueConstants } from '../../../utilities/fizz-buzz-constants';
import '../../../setupTests';
import { sequenceGenerator } from './../../../utilities';

// Create the mock store
import configureMockStore from 'redux-mock-store';
const mockStore = configureMockStore();

const initialState = {
  currentPage: 1,
  currentDisplayCount: 0,
};

const props = {
  computeFizzBuzzValues: () => {},
  resetValues: () => {},
};

const store = mockStore(initialState);

describe('FormContent Component', () => {
  describe('Checking PropsTypes', () => {
    it('Should not throw errow when default props are sent', () => {
      const expectedProps = {
        computeFizzBuzzValues: () => {},
        resetValues: () => {},
      };
      var propsError = checkProps(FormContent, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Checking Props', () => {
    let wrapper;

    const initialState = {
      error: false,
      errorMsg: '',
      userInput: 123,
    };

    const store = mockStore(initialState);
    // Shallow render the container passing in the mock store

    it('Should have props', () => {
      const renderComponent = (props = {}) =>
        shallow(<FormContent store={store} {...props} />).dive();

      const dispatchMock = jest.fn();
      const component = renderComponent({ dispatch: dispatchMock });
      const formElement = component.find(IntegerInput);
      expect(formElement.length).toEqual(1);
    });

    it('Should have no props', () => {
      wrapper = shallow(<FormContent store={store} />);
      const formElement = dataTestAttributeFinder(wrapper, 'formContentElement');
      expect(formElement.length).toEqual(0);
    });
  });

  describe('Checking Child Components', () => {
    const renderComponent = (props = {}) =>
      shallow(<FormContent store={store} {...props} />).dive();

    const dispatchMock = jest.fn();
    const component = renderComponent({ dispatch: dispatchMock });

    it('Should render IntegerInput component', () => {
      const integerInputElement = component.find(IntegerInput);
      expect(integerInputElement.exists()).toBe(true);
    });

    it('Should render Button component', () => {
      const buttonElement = component.find(Button);
      expect(buttonElement.exists()).toBe(true);
    });
  });

  describe('Checking Functions of FormContent for valid input', () => {
    const setState = jest.fn();

    const state = {
      userInput: 16,
    };

    jest
      .spyOn(React, 'useState')
      .mockImplementation((stateValue) => [(stateValue = state.userInput), setState]);

    const renderComponent = (props = {}) =>
      shallow(<FormContent store={store} {...props} />).dive();

    const dispatchMock = jest.fn();
    const component = renderComponent({ dispatch: dispatchMock });

    it('should set state for valid user input and day is Wednesday', () => {
      const functionResultSequenceGenerator = sequenceGenerator(15, true); // set the day as true for Ex: wednesday if it needs output alterations
      expect(functionResultSequenceGenerator[14].value).toBe(FizzBuzzValueConstants.wizzWuzz); // check the 15 number to return wizz wizz
    });

    it('should simulate a form change function for a use input', () => {
      const wrapper = mount(<FormContent store={store} />);
      component.find('IntegerInput').simulate('change', { target: { value: 16 } });
      expect(component.find('IntegerInput').prop('value')).toEqual(16);
      wrapper.find('Button').simulate('click');

      component.find('IntegerInput').simulate('keydown', {
        key: 'Enter',
        keyCode: 13,
        preventDefault: jest.fn(),
      });
    });
  });

  describe('Checking Functions of FormContent for error scenario', () => {
    const setState = jest.fn();
    const stateError = {
      userInput: -1,
    };
    jest
      .spyOn(React, 'useState')
      .mockImplementation((stateValue) => [(stateValue = stateError.userInput), setState]);

    const renderComponent = (props = {}) =>
      shallow(<FormContent store={store} {...props} />).dive();

    const dispatchMock = jest.fn();
    const component = renderComponent({ dispatch: dispatchMock });

    it('should simulate a form change function for a use input', () => {
      const wrapper = mount(<FormContent store={store} />);
      wrapper.find('Button').simulate('click');
      expect(component.find('IntegerInput').prop('value')).toEqual(-1);
      component.find('IntegerInput').simulate('keydown', {
        key: 'Enter',
        keyCode: 13,
        preventDefault: jest.fn(),
      });
    });
  });

  describe('Checking dispatches from the FormContent Component', () => {
    beforeEach(() => {
      // Runs before each test in the suite
      store.clearActions();
    });

    describe('Checking dispatches', () => {
      it('Dispatches the correct action and payload : RESET_FIZZ_VALUES', () => {
        const expectedActions = [
          {
            type: types.RESET_FIZZ_VALUES,
          },
        ];

        store.dispatch(actions.resetValues());
        expect(store.getActions()).toEqual(expectedActions);
      });

      it('Dispatches the correct action and payload : COMPUTE_FIZZ_VALUES', () => {
        const payload = [
          {
            key: 1,
            value: 1,
          },
          {
            key: 2,
            value: 2,
          },
          {
            key: 3,
            value: 'fizz',
          },
          {
            key: 4,
            value: 4,
          },
          {
            key: 5,
            value: 'buzz',
          },
        ];
        const expectedActions = [
          {
            type: types.COMPUTE_FIZZ_VALUES,
            payload,
          },
        ];

        store.dispatch(actions.computeValues(payload));
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
    describe('Checking mapDispatchToProps', () => {
      const dispatch = jest.fn();

      const data = [
        {
          key: 1,
          value: 1,
        },
        {
          key: 2,
          value: 2,
        },
        {
          key: 3,
          value: 'fizz',
        },
        {
          key: 4,
          value: 4,
        },
        {
          key: 5,
          value: 'buzz',
        },
      ];
      it('it should call correct dispatches', () => {
        const actualValue = mapDispatchToProps(dispatch);
        expect(Object.keys(actualValue)).toEqual(['computeFizzBuzzValues', 'resetValues']);

        actualValue.computeFizzBuzzValues(data);
        expect(dispatch).toBeCalledWith(computeValues(data));

        actualValue.resetValues();
        expect(dispatch).toBeCalledWith(resetValues());
      });
    });
  });
});
