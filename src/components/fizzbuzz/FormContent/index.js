import React from 'react';
import {
  FizzBuzzConstants,
  FizzBuzzValidationConstants,
} from '../../../utilities/fizz-buzz-constants';
import PropTypes from 'prop-types';
import Button from '../../core/Button';
import IntegerInput from '../../core/IntegerInput';
import { computeValues, resetValues } from './../../../redux/action';
import { sequenceGenerator } from './../../../utilities';
import { connect } from 'react-redux';

const { minValue, maxValue, errorMessage } = FizzBuzzValidationConstants;

const propTypes = {
  computeFizzBuzzValues: PropTypes.func,
  resetValues: PropTypes.func,
};

const defaultProps = {
  computeFizzBuzzValues: () => {},
  resetValues: () => {},
};

const FormContent = (props) => {
  const [userInput, setUserInput] = React.useState('');
  const [errorOnInput, setErrorOnInput] = React.useState(false);
  const [errorMsg, setErrorMsg] = React.useState('');

  const onChangeHandler = (e) => {
    let eVal = e.target.value;
    setUserInput(eVal);
  };

  const stateSetterHandler = (isError, errorMessage) => {
    props.resetValues(); //Resetting the previously generated values
    setErrorOnInput(isError);
    setErrorMsg(errorMessage);
  };

  const onKeyDownHandler = (e) => {
    if (e.keyCode === 13 || e.keyCode === 32) {
      onClickHandler(e);
    }
  };

  const onClickHandler = (event) => {
    event.preventDefault();
    const { minValue, maxValue, errorMessage } = FizzBuzzValidationConstants;

    let eVal = userInput;
    if (eVal >= minValue && eVal <= maxValue) {
      stateSetterHandler(false, '');
      props.computeFizzBuzzValues(sequenceGenerator(eVal));
    } else {
      stateSetterHandler(true, errorMessage);
    }
  };

  const { buttonText } = FizzBuzzConstants;

  return (
    <>
      <form className="form-group w-25" data-test="formContentElement">
        {errorOnInput && (
          <div className="my-2 alert alert-danger" id="errorMessage" role="alert">
            {errorMsg}
          </div>
        )}
        <IntegerInput
          type="number"
          name="inputVal"
          id="inputVal"
          className="form-control"
          value={userInput}
          describeBy="errorMessage"
          onChange={onChangeHandler}
          onKeyDown={onKeyDownHandler}
          minValue={minValue}
          maxValue={maxValue}
        />
        <Button
          onClickHandler={onClickHandler}
          buttonText={buttonText}
          disabled={`${userInput === '' ? 'disabled' : ''}`}
        />
      </form>
    </>
  );
};

export const mapDispatchToProps = (dispatch) => ({
  computeFizzBuzzValues: (data) => {
    dispatch(computeValues(data));
  },
  resetValues: () => {
    dispatch(resetValues());
  },
});

FormContent.propTypes = propTypes;
FormContent.defaultProps = defaultProps;

export default connect(null, mapDispatchToProps)(FormContent);
