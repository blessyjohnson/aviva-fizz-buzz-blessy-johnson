import React from 'react';
import PropTypes from 'prop-types';
import { FizzBuzzConstants } from '../../../utilities/fizz-buzz-constants';
import Button from '../../core/Button';

const propTypes = {
  currentPage: PropTypes.number.isRequired,
  pageCount: PropTypes.number.isRequired,
  previousPage: PropTypes.func,
  nextPage: PropTypes.func,
  previousText: PropTypes.string,
  nextText: PropTypes.string,
};

const defaultProps = {
  currentPage: 0,
  pageCount: -1,
  previousPage: undefined,
  nextPage: undefined,
  previousText: '',
  nextText: '',
};

const { previousText, nextText } = FizzBuzzConstants;

const Pagination = (props) => {
  if (Object.keys(props).length === 0 || props.pageCount === -1) {
    return null;
  }

  const { previousPage, nextPage, pageCount, currentPage, previousText, nextText } = props;
  return (
    <>
      <section
        data-test="paginationComponent"
        aria-label="Pagination navigation"
        className={`${currentPage === 1 && pageCount === 1 ? 'hide' : ''}`}
      >
        <Button
          disabled={`${currentPage === 1 ? 'disabled' : ''}`}
          onClickHandler={previousPage}
          buttonText={previousText}
          ariaLabel="Go to previous page"
        />
        <span className="px-3 pagecount">
          {currentPage} of {pageCount}
        </span>
        <Button
          disabled={`${currentPage === pageCount ? 'disabled' : ''}`}
          onClickHandler={nextPage}
          buttonText={nextText}
          ariaLabel="Go to next page"
        />
      </section>
    </>
  );
};

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;

export default Pagination;
