import React from 'react';
import { shallow } from 'enzyme';
import Pagination from './index';
import Button from './../../core/Button';
import { checkProps, dataTestAttributeFinder } from '../../../../Utils';

const setup = (props = {}) => {
  const wrapper = shallow(<Pagination {...props} />);
  return wrapper;
};

describe('Pagination Component', () => {
  const props = {
    currentPage: 0,
    pageCount: 0,
    previousPage: () => {},
    nextPage: () => {},
    previousText: '',
    nextText: '',
  };

  describe('Checking Proptypes', () => {
    it('When we pass a default prop, it should not throw any error', () => {
      const expectedProps = {
        currentPage: 0,
        pageCount: 0,
        previousPage: () => {},
        nextPage: () => {},
        previousText: '',
        nextText: '',
      };

      const propsError = checkProps(Pagination, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Checking props', () => {
    it('Should render with props', () => {
      const wrapper = setup(props);
      const paginationElement = dataTestAttributeFinder(wrapper, 'paginationComponent');
      expect(paginationElement.length).toEqual(1);
    });
  });

  describe('Checking props', () => {
    it('Should render with no props', () => {
      const wrapper = setup(undefined);
      const paginationElement = dataTestAttributeFinder(wrapper, 'paginationComponent');
      expect(paginationElement.length).toEqual(0);
    });
  });

  describe('Checking Child components', () => {
    const wrapper = setup(props);
    it('Should render Button Component', () => {
      const buttonComponent = wrapper.find(Button);
      expect(buttonComponent.exists()).toBe(true);
    });
  });
});
