import React from 'react';
import { shallow } from 'enzyme';
import Header from './index';
import { checkProps, dataTestAttributeFinder } from '../../../../Utils';

describe('Header Component: Visual', () => {
  const props = {
    heading: 'Test heading',
  };

  it('should render a heading', () => {
    const component = shallow(<Header {...props} />);
    const heading = component.find('h1').text();
    expect(heading).toBe('Test heading');
  });
});

describe('Header Component : Functional', () => {
  describe('Checking prop types', () => {
    it('When we pass a default prop, it should not throw any error', () => {
      const expectedProps = {
        heading: 'Test heading',
      };

      const propsError = checkProps(Header, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Have props', () => {
    const props = {
      heading: 'Test heading',
    };

    it('Should render with props', () => {
      const wrapper = shallow(<Header {...props} />);
      const headerElement = dataTestAttributeFinder(wrapper, 'headerComponent');
      expect(headerElement.length).toEqual(1);
    });
  });

  describe('Have no props', () => {
    const wrapper = shallow(<Header />);

    it('should render nothing, since no props', () => {
      const headerElement = dataTestAttributeFinder(wrapper);
      expect(headerElement.length).toEqual(0);
    });
  });
});
