import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  heading: PropTypes.string.isRequired,
};

const defaultProps = {
  heading: '',
};

const Header = (props) => {
  const { heading } = props;
  return (
    <div className="py-2 text-center" data-test="headerComponent">
      <h1>{heading}</h1>
    </div>
  );
};

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;
