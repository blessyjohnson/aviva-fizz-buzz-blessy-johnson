import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  userInput: PropTypes.number,
  onChange: PropTypes.func,
  onKeyDown: PropTypes.func,
  describeBy: PropTypes.string,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,
};
const defaultProps = {
  type: '',
  className: '',
  name: '',
  id: '',
  userInput: undefined,
  onChange: undefined,
  onKeyDown: undefined,
  describeBy: '',
  minValue: undefined,
  maxValue: undefined,
};

const IntegerInput = (props) => {
  if (Object.keys(props).length === 0 || props.type === '') {
    return null;
  }

  const {
    type,
    name,
    id,
    className,
    userInput,
    onChange,
    onKeyDown,
    describeBy,
    minValue,
    maxValue,
  } = props;

  return (
    <>
      <label htmlFor="inputVal" className="form-label">
        Enter a value between {minValue} and {maxValue}
      </label>
      <input
        data-test="inputElement"
        type={type}
        name={name}
        id={id}
        aria-describedby={describeBy}
        className={className}
        autoFocus
        value={userInput}
        onChange={onChange}
        onKeyDown={onKeyDown}
      />
    </>
  );
};

IntegerInput.propTypes = propTypes;
IntegerInput.defaultProps = defaultProps;

export default IntegerInput;
