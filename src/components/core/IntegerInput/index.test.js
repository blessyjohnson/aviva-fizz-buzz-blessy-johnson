import React from 'react';
import { shallow } from 'enzyme';
import IntegerInput from './index';
import { checkProps, dataTestAttributeFinder } from './../../../../Utils';

describe('IntegerInput Component', () => {
  describe('Checking Proptypes', () => {
    it('When we pass a default prop, it should not throw any error', () => {
      const expectedProps = {
        type: 'Test type',
        name: 'Test Name',
        id: 'Test id',
        className: 'Test class',
        userInput: 123,
        onChange: () => {},
        onKeyDown: () => {},
      };
      const propsError = checkProps(IntegerInput, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Have props', () => {
    const props = {
      type: 'Test Type',
      name: 'Test Name',
      id: 'Test id',
      className: 'Test class',
      userInput: 123,
      onChange: () => {},
      onKeyDown: () => {},
    };
    const wrapper = shallow(<IntegerInput {...props} />);

    it('Should have props', () => {
      const inputElement = dataTestAttributeFinder(wrapper, 'inputElement');
      expect(inputElement.length).toEqual(1);
    });

    it('should return with positive reference for input element when props are passed', () => {
      const component = shallow(<IntegerInput {...props} />);
      const inputElement = component.find('input');
      expect(inputElement.exists()).toBe(true);
    });
  });

  describe('Have no props', () => {
    const wrapper = shallow(<IntegerInput />);

    it('Should have not return content', () => {
      const inputElement = dataTestAttributeFinder(wrapper, 'inputElement');
      expect(inputElement.length).toEqual(0);
    });
  });
});
