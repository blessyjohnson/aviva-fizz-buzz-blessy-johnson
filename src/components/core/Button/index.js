import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  buttonText: PropTypes.string.isRequired,
  onClickHandler: PropTypes.func,
  disabled: PropTypes.string,
};

const defaultProps = {
  buttonText: '',
  onClickHandler: undefined,
  disabled: undefined,
};

const Button = (props) => {
  const { buttonText, onClickHandler, disabled, ariaLabel } = props;

  if (buttonText === '') return null;

  return (
    <button
      data-test="buttonComponent"
      type="button"
      className="btn btn-primary mt-4"
      onClick={onClickHandler}
      aria-label={ariaLabel}
      title={buttonText}
      disabled={disabled}
    >
      {buttonText}
    </button>
  );
};

Button.propTypes = propTypes;
Button.defaultProps = defaultProps;

export default Button;
