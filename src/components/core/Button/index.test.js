import React from 'react';
import { shallow } from 'enzyme';
import Button from './index';
import { checkProps, dataTestAttributeFinder } from './../../../../Utils';

describe('Button Component : Cosmetics', () => {
  const wrapper = shallow(<Button buttonText="Generate Values" />);

  it('should render a button', () => {
    const buttonTitle = wrapper.find('button').text();
    expect(buttonTitle).toBe('Generate Values');
  });

  it('should render a button with type button', () => {
    const buttonType = wrapper.find('button').type();
    expect(buttonType).toBe('button');
  });
});

describe('Button Component : Functional', () => {
  describe('Checking prop types', () => {
    it('When we pass a default prop, it should not throw any error', () => {
      const expectedProps = {
        onClickHandler: () => {},
        buttonText: 'Test Button text',
        disabled: 'disabled',
      };

      const propsError = checkProps(Button, expectedProps);
      expect(propsError).toBeUndefined();
    });
  });

  describe('Have props', () => {
    const props = {
      onClickHandler: () => {},
      buttonText: 'Test Button text',
      disabled: 'disabled',
    };

    it('Should render with props', () => {
      const wrapper = shallow(<Button {...props} />);
      const buttonElement = dataTestAttributeFinder(wrapper, 'buttonComponent');
      expect(buttonElement.length).toEqual(1);
    });
  });

  describe('Have no props', () => {
    const wrapper = shallow(<Button />);

    it('should render nothing, since no props', () => {
      const buttonElement = dataTestAttributeFinder(wrapper);
      expect(buttonElement.length).toEqual(0);
    });
  });
});
