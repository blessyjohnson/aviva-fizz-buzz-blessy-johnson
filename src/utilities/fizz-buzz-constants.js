const FizzBuzzValidationConstants = {
  minValue: 1,
  maxValue: 1000,
  errorMessage: 'Please enter a valid number',
};

const FizzBuzzConstants = {
  headerText: 'FIZZ BUZZ CHALLENGE',
  buttonText: 'Generate Values',
  previousText: 'Previous',
  nextText: 'Next',
  displayCount: 20,
};

const FizzBuzzValueConstants = {
  fizz: 'fizz',
  buzz: 'buzz',
  fizzBuzz: 'fizz buzz',
  wizz: 'wizz',
  wuzz: 'wuzz',
  wizzWuzz: 'wizz wuzz',
};

export { FizzBuzzConstants, FizzBuzzValueConstants, FizzBuzzValidationConstants };
