import { FizzBuzzValueConstants } from './fizz-buzz-constants';

const { wizzWuzz, wizz, wuzz, fizzBuzz, fizz, buzz } = FizzBuzzValueConstants;

export const getNewDate = () => {
  return new Date().getDay();
};

export const isDayApplicable = () => {
  const day = getNewDate();
  return day === 3; // Return true if its 3 / wednesday for output alterations
};

export const sequenceGenerator = (eVal, day) => {
  let values = [];

  if (day === undefined) {
    day = isDayApplicable();
  }

  for (let i = 1; i <= eVal; i++) {
    let computedVal;

    i % 5 === 0 && i % 3 === 0
      ? (computedVal = day ? wizzWuzz : fizzBuzz)
      : i % 3 === 0
      ? (computedVal = day ? wizz : fizz)
      : i % 5 === 0
      ? (computedVal = day ? wuzz : buzz)
      : (computedVal = i);

    values.push({ key: i, value: computedVal });
  }
  return values;
};
